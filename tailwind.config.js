/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        "yekanRegular": "yekanRegular",
        "yekanBold": "yekanBold",
        "poppins": "poppins"
      },
      colors: {
        "primary": "#060047",
        "secondary": "#B3005E",
        "tertiary": "#7366FF"
      }
    },
  },
  plugins: [],
}

