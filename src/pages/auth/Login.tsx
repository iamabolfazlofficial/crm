import { useState, useCallback } from "react";
import Input from "../../components/Input";
import { toAbsoluteUrl } from "../../_cloner/utils/Helper";
import Button from "../../components/Button";
const Login = () => {
    const [showPassword, setShowPassword] = useState<boolean>(false);

    const initialValue = { username: "", password: "" };

    const [inputs, setInputs] = useState(initialValue);
    const onChangeHandler = useCallback(
        ({ target: { name, value } }: any) =>
            setInputs((state) => ({ ...state, [name]: value })),
        []
    );

    return (
        <section className="grid min-h-screen w-full grid-cols-3">
            {/* First Section */}
            <div className="flex-center flex-col">
                <header className="flex-center flex-col pb-8">
                    <img
                        src={toAbsoluteUrl("/media/images/saipa-logo.png")}
                        alt="Saipa Logo"
                    />
                    <div className="pt-8">
                        <span className="font-yekanRegular text-sm text-tertiary">
                            برای ورود به سیستم لطفا نام کاربری و کلمه عبور خود
                            را وارد نمایید
                        </span>
                    </div>
                </header>
                <main>
                    <form className="flex-justify-center flex-col gap-4">
                        <section>
                            <Input
                                type="text"
                                title="نام کاربری"
                                value={inputs.username}
                                name="username"
                                onChange={onChangeHandler}
                                icon={toAbsoluteUrl("/media/images/user.png")}
                                material={true}
                            />
                        </section>
                        <section>
                            <Input
                                type={showPassword ? "text" : "password"}
                                title="کلمه عبور"
                                value={inputs.password}
                                name="password"
                                onChange={onChangeHandler}
                                password={showPassword}
                                passwordState={setShowPassword}
                                icon={toAbsoluteUrl(
                                    `${
                                        showPassword
                                            ? "/media/images/eye.png"
                                            : "/media/images/eye-crossed.png"
                                    }`
                                )}
                                material={true}
                            />
                        </section>
                        <div>
                            <span className="font-yekanBold text-xs text-secondary">
                                رمز عبور خود را فراموش کرده ام
                            </span>
                        </div>
                        <Button
                            type="SUBMIT"
                            value="ورود"
                            className="primary-gradient cursor-pointer rounded-full p-2 font-yekanRegular text-white shadow-xl"
                        />
                    </form>
                </main>
                <footer className="ltr pt-8 font-poppins text-sm text-primary">
                    &copy; 2023 - Saipacorp.com
                </footer>
            </div>
            {/* Second Section */}
            <div className="col-span-2 bg-primary"></div>
        </section>
    );
};

export default Login;
