import { FC } from "react";

interface IProps {
    value: string;
    className: string;
    type: string;
}

const Button: FC<IProps> = ({ value, className, type }) => {
    
    const rendered = (): JSX.Element | undefined => {
        switch (type) {
            case "SUBMIT":
                return (
                    <input value={value} type="submit" className={className} />
                );
            default:
                break;
        }
    };

    return <>{rendered()}</>;
};

export default Button;
