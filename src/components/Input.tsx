import { FC } from "react";

interface IProps {
    value: string | number;
    onChange: React.ChangeEventHandler<HTMLInputElement> | undefined;
    name: string;
    title: string;
    type: string;
    icon?: string;
    passwordState?: React.Dispatch<React.SetStateAction<boolean>>;
    password?: boolean;
    material?: boolean;
}

const Input: FC<IProps> = ({
    type,
    value,
    onChange,
    name,
    title,
    icon,
    passwordState,
    password,
    material,
}) => {
    return (
        <>
            {material ? (
                <div className="relative">
                    <input
                        type={type}
                        name={name}
                        value={value}
                        onChange={onChange}
                        className="materialInput__input box-border h-[48px] w-[320px] rounded border border-solid border-primary p-4 font-yekanRegular focus:border-tertiary focus:outline-none"
                        aria-labelledby="label-input"
                        autoComplete="false"
                    />
                    <label
                        className="materialInput__label pointer-events-none absolute bottom-0 right-4 top-0 flex items-center"
                        id="label-input"
                    >
                        <div className="materialInput__label-text font-yekanRegular text-sm">
                            {title}
                        </div>
                    </label>
                    {icon && (
                        <img
                            className="absolute left-4 top-4"
                            alt="Icon"
                            src={icon}
                            width={18}
                            height={18}
                            onClick={() =>
                                passwordState && passwordState(!password)
                            }
                        />
                    )}
                </div>
            ) : null}
        </>
    );
};

export default Input;
