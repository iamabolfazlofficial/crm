import { Route, Routes } from "react-router";
import Login from "./pages/auth/Login";
import Dashboard from "./pages/admin/dashboard/Dashboard";

const App = () => {
  return (
    <Routes>
      {/* Admins */}
      <Route path="admin/dashboard" element={<Dashboard />} />
      {/* Admins */}
      <Route path="auth/login" element={<Login />} />
    </Routes>
  )
}

export default App